Commerce Shipping Carrier
--------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Commerce Shipping Carrier module is a simple module that allows you
to add your own shipping carriers and configure the URL patterns to
create tracking links similar to what was offered in the simple_package_tracking
module for D7. This helps especially where flat rate shipping methods
don't support tracking urls.


REQUIREMENTS
------------

This module requires the following modules:

  * [Commerce](https://www.drupal.org/project/commerce)
  * [Commerce Shipping](https://www.drupal.org/project/commerce_shipping)


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module.
    Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

  * Go to Configuration >> Shipping >> Carriers,
    or @ /admin/commerce/config/shipping_carriers to create a carrier.
    In the URL pattern, Use [tracking_code] where the tracking number
    should appear in the URL for that specific carrier.
    The carriers you create will be available to select from when
    you create shipments for an order. They will take the tracking number you provide,
    and format the number into a tracking link.

    You can also add {{ shipment_entity.shipping_carrier.entity.label }}
    to the commerce-shipment-confirmation.html.twig template file
    in a custom module to display the shipping carrier in the shipping confirmation email.

    Or something like this:

    {% if shipment_entity.shipping_carrier.target_id is not null %}
      'Shipping carrier:'
      {{ shipment_entity.shipping_carrier.entity.label }}
    {% endif %}

MAINTAINERS
-----------

Current maintainers:
 * Tony Ferguson https://www.drupal.org/u/tonytheferg
