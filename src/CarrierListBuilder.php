<?php

namespace Drupal\commerce_shipping_carrier;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of shipping carriers.
 */
class CarrierListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Carrier');
    $header['url_pattern'] = $this->t('URL Pattern');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_shipping_carrier\Entity\CarrierInterface $entity */
    $row['label'] = $entity->label();
    $row['url_pattern'] = $entity->getUrlPattern();

    return $row + parent::buildRow($entity);
  }

}
