<?php

namespace Drupal\commerce_shipping_carrier\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the carrier add/edit form.
 */
class CarrierForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\commerce_shipping_carrier\Entity\CarrierInterface $carrier */
    $carrier = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carrier Name'),
      '#maxlength' => 255,
      '#default_value' => $carrier->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $carrier->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_shipping_carrier\Entity\Carrier::load',
      ],
      '#disabled' => !$carrier->isNew(),
    ];
    $form['url_pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Pattern'),
      '#maxlength' => 255,
      '#default_value' => $carrier->getUrlPattern(),
      '#description' => $this->t('Create the URL pattern to be used for the tracking code link. Use [tracking_code] where the tracking number should appear in the URL.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $carrier = $this->entity;
    $status = $carrier->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label carrier.', [
          '%label' => $carrier->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label carrier.', [
          '%label' => $carrier->label(),
        ]));
    }
    $form_state->setRedirect('entity.commerce_shipping_carrier.collection');

    return $status;
  }

}
