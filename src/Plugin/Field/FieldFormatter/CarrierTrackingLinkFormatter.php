<?php

namespace Drupal\commerce_shipping_carrier\Plugin\Field\FieldFormatter;

use Drupal\commerce_shipping\Plugin\Field\FieldFormatter\TrackingLinkFormatter;
use Drupal\commerce_shipping_carrier\Entity\CarrierInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formats tracking links by carrier.
 */
class CarrierTrackingLinkFormatter extends TrackingLinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($items->isEmpty()) {
      return [];
    }
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $items[0]->getEntity();
    if ($shipment->get('shipping_carrier')->isEmpty()) {
      return parent::viewElements($items, $langcode);
    }
    $shipping_carrier = $shipment->get('shipping_carrier')->entity;
    if (!$shipping_carrier instanceof CarrierInterface) {
      return parent::viewElements($items, $langcode);
    }
    $tracking_url = $shipping_carrier->getTrackingUrl($shipment);
    if (!$tracking_url) {
      return [['#markup' => Xss::filterAdmin((string) $shipment->getTrackingCode())]];
    }
    $elements = [];
    $elements[] = [
      '#type' => 'link',
      '#title' => $shipment->getTrackingCode(),
      '#url' => $tracking_url,
    ];

    return $elements;
  }

}
