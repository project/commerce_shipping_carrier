<?php

namespace Drupal\commerce_shipping_carrier\Entity;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for package type configuration entities.
 */
interface CarrierInterface extends ConfigEntityInterface {

  /**
   * Gets the carrier url_pattern.
   *
   * @return array
   *   An array with the following keys: length, width, height, unit.
   */
  public function getUrlPattern();

  /**
   * Get the tracking URL for a shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return \Drupal\core\Url|null
   *   The tracking URL, or NULL if not available yet.
   */
  public function getTrackingUrl(ShipmentInterface $shipment);

}
