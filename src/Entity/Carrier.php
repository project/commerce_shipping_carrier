<?php

namespace Drupal\commerce_shipping_carrier\Entity;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Url;

/**
 * Defines the carrier entity class.
 *
 * @ConfigEntityType(
 *   id = "commerce_shipping_carrier",
 *   label = @Translation("Carrier"),
 *   label_collection = @Translation("Carriers"),
 *   label_singular = @Translation("Carrier"),
 *   label_plural = @Translation("Carriers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Carrier",
 *     plural = "@count Carriers",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\commerce_shipping_carrier\CarrierListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_shipping_carrier\Form\CarrierForm",
 *       "edit" = "Drupal\commerce_shipping_carrier\Form\CarrierForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "commerce_shipping_carrier",
 *   admin_permission = "administer commerce_shipping_carrier",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "url_pattern",
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/config/shipping_carriers/add",
 *     "edit-form" = "/admin/commerce/config/shipping_carriers/manage/{commerce_shipping_carrier}",
 *     "delete-form" = "/admin/commerce/config/shipping_carriers/manage/{commerce_shipping_carrier}/delete",
 *     "collection" = "/admin/commerce/config/shipping_carriers"
 *   }
 * )
 */
class Carrier extends ConfigEntityBase implements CarrierInterface {

  /**
   * The carrier ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The carrier label.
   *
   * @var string
   */
  protected $label;

  /**
   * The carrier url pattern.
   *
   * @var array
   */
  protected $url_pattern;

  /**
   * {@inheritdoc}
   */
  public function getUrlPattern() {
    return $this->url_pattern;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $code = $shipment->getTrackingCode();
    if (empty($code)) {
      return NULL;
    }
    try {
      $url = str_replace('[tracking_code]', $code, $this->getUrlPattern());
      return Url::fromUri($url);
    }
    catch (\InvalidArgumentException $e) {
      return NULL;
    }
  }

}
