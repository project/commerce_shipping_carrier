<?php

/**
 * @file
 * Provides shipping carrier functionality.
 */

use Drupal\commerce_shipping_carrier\Plugin\Field\FieldFormatter\CarrierTrackingLinkFormatter;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_entity_base_field_info().
 */
function commerce_shipping_carrier_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() === 'commerce_shipment') {
    $fields['shipping_carrier'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Shipping carrier'))
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setDescription(t('The shipping carrier'))
      ->setSetting('target_type', 'commerce_shipping_carrier')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }
  return $fields;
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function commerce_shipping_carrier_field_formatter_info_alter(array &$info) {
  if (isset($info['commerce_tracking_link'])) {
    $info['commerce_tracking_link']['class'] = CarrierTrackingLinkFormatter::class;
  }
}
